
import { AntdSkInput }  from '../../sk-input-antd/src/antd-sk-input.js';


export class Antd4SkInput extends AntdSkInput {

    get prefix() {
        return 'antd4';
    }

}
